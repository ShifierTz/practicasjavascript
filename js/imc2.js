function calcularIMC() {
    var altura = parseFloat(document.getElementById("altura").value);
    var peso = parseFloat(document.getElementById("peso").value);
    var edad = parseInt(document.getElementById("edad").value);
    var genero = document.querySelector('input[name="sexo"]:checked');
    
    if (!isNaN(altura) && !isNaN(peso) && !isNaN(edad) && genero!=null) {
        var imc = peso / (altura * altura);
        document.getElementById("resultado").textContent = imc.toFixed(2);
        imagen(genero.value,imc);
        calorias(genero.value,edad,peso);
    } else {
        alert("Ingresa valores válidos para edad, género, altura y peso.");
    }
}

function limpiar(){
    document.getElementById("altura").value = "";
    document.getElementById("peso").value = "";
    document.getElementById("resultado").textContent = "";
    document.getElementById("edad").value = "";
    document.getElementById("calorias").textContent = "";
}

function imagen(generoa, imc){
    var imagenDiv = document.querySelector(".imagen");
    var imagenUrl = "";

    if(generoa === "hombre"){
        if(imc<18.5){
            imagenUrl = "/img/01H.png";
        } else if(imc<25){
            imagenUrl = "/img/02H.png";
        } else if(imc<30){
            imagenUrl = "/img/03H.png";
        } else if(imc<35){
            imagenUrl = "/img/04H.png";
        } else if(imc<40){
            imagenUrl = "/img/05H.png";
        } else {
            imagenUrl = "/img/06H.png";
        }
    } else {
        if(imc<18.5){
            imagenUrl = "/img/01M.png";
        } else if(imc<25){
            imagenUrl = "/img/02M.png";
        } else if(imc<30){
            imagenUrl = "/img/03M.png";
        } else if(imc<35){
            imagenUrl = "/img/04M.png";
        } else if(imc<40){
            imagenUrl = "/img/05M.png";
        } else {
            imagenUrl = "/img/06M.png";
        }
    }

    imagenDiv.style.backgroundImage = "url('" + imagenUrl + "')";
}

function calorias(generob,edad,peso){
    var cadena;
    var cal;
    if(generob === "hombre"){
        if(edad < 10){
            cadena = "Se recomienda consultar al pediatra para la alimentaciín del infante";
        } else if(edad <18){
            cal = (17.686*peso)+658.2;
            cadena = "Se recomienda consumir: "+cal+" calorías";
        } else if(edad <30){
            cal = (15.057*peso)+692.2;
            cadena = "Se recomienda consumir: "+cal+" calorías";
        } else if(edad <60){
            cal = (11.472*peso)+873.1;
            cadena = "Se recomienda consumir: "+cal+" calorías";
        } else if(edad >= 60){
            cal = (11.711*peso)+587.7;
            cadena = "Se recomienda consumir: "+cal+" calorías";
        }
    } else{
        if(edad < 10){
            cadena = "Se recomienda consultar al pediatra para la alimentación del infante";
        } else if(edad <18){
            cal = (13.384*peso)+692.6;
            cadena = "Se recomienda consumir: "+cal+" calorías";
        } else if(edad <30){
            cal = (14.818*peso)+486.6;
            cadena = "Se recomienda consumir: "+cal+" calorías";
        } else if(edad <60){
            cal = (8.126*peso)+845.6;
            cadena = "Se recomienda consumir: "+cal+" calorías";
        } else if(edad >= 60){
            cal = (9.082*peso)+658.5;
            cadena = "Se recomienda consumir: "+cal+" calorías";
        }
    }

    document.getElementById("calorias").textContent = cadena; 
}

